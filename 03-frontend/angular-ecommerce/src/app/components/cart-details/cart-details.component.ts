import { Component, OnInit } from '@angular/core';
import { CartItem } from 'src/app/common/cart-item';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.css']
})
export class CartDetailsComponent implements OnInit {

  cartItems: CartItem[] = [];
  
  totalPrice: number = 0.00;
  totalQuantity: number = 0;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.listCartDetails();
  }

  listCartDetails() {
    
    // load cart items from cart service
    this.cartItems = this.cartService.cartItems;

    // subscribe to total price from cart service
    this.cartService.totalPrice.subscribe(
      data => this.totalPrice = data
    );

    // subscribe to total quantity from cart service
    this.cartService.totalQuantity.subscribe(
      data => this.totalQuantity = data
    );

    // cart service push newest data to all subcribers, include this component
    this.cartService.computeCartTotals();

  }

  incrementQuantity(theCartItem: CartItem) {
    this.cartService.addToCart(theCartItem);
  }

  decrementQuantity(theCartItem: CartItem) {
    this.cartService.decrementQuantity(theCartItem);
  }

  removeCartItem(theCartItem: CartItem) {
    this.cartService.remove(theCartItem);
  }

}
