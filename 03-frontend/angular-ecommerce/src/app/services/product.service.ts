import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../common/product';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProductCategory } from '../common/product-category';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl = 'http://localhost:8080/api/products';

  private categoryUrl = 'http://localhost:8080/api/product-category';

  constructor(private httpClient: HttpClient) { }

  getProductListPaginate(thePage: number,
    thePageSize: number,
    theCategoryId: number): Observable<GetResponseProducts> {

    let searchUrl = '';
    // need to build URL based on category id , page number and page size
    if (theCategoryId == 0) {
      searchUrl = `${this.baseUrl}`
        + `?page=${thePage}&size=${thePageSize}`;
    } else {
      searchUrl = `${this.baseUrl}/search/findByCategoryId?id=${theCategoryId}`
        + `&page=${thePage}&size=${thePageSize}`;
    }

    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  getProduct(productId: string): Observable<Product> {
    // url
    const url = this.baseUrl + "/" + productId;
    return this.httpClient.get<Product>(url);
  }

  searchProducts(q: string): Observable<Product[]> {
    // need to build URL based on q
    const searchUrl = `${this.baseUrl}/search/findByNameContaining?name=${q}`;

    return this.getProducts(searchUrl);
  }

  searchProductsPaginate(thePage: number,
    thePageSize: number,
    q: string): Observable<GetResponseProducts> {
    // url
    let searchUrl = `${this.baseUrl}/search/findByNameContaining?name=${q}&page=${thePage}&size=${thePageSize}`;

    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  getProductListByCategoryId(theCategoryId: number): Observable<Product[]> {

    // need to build URL based on category id 
    const searchUrl = `${this.baseUrl}/search/findByCategoryId?id=${theCategoryId}`;

    return this.getProducts(searchUrl);
  }

  //------------------BEGIN UNUSED------------------
  getAllProductList(): Observable<Product[]> {

    // need to build URL
    const url = `${this.baseUrl}`;

    return this.getProducts(url);
  }
  //------------------END UNUSED------------------

  private getProducts(searchUrl: string): Observable<Product[]> {
    return this.httpClient.get<GetResponseProducts>(searchUrl).pipe(
      map(response => response._embedded.products)
    );
  }

  getProductCategories(): Observable<ProductCategory[]> {

    return this.httpClient.get<GetResponseProductCategory>(this.categoryUrl).pipe(
      map(response => response._embedded.productCategory)
    );
  }

}

interface GetResponseProducts {
  _embedded: {
    products: Product[];
  },
  page: {
    size: number;
    totalElements: number,
    totalPages: number,
    number: number
  }
}

interface GetResponseProductCategory {
  _embedded: {
    productCategory: ProductCategory[];
  }
}